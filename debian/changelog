r-bioc-phyloseq (1.50.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 12 Jan 2025 20:18:41 +0100

r-bioc-phyloseq (1.50.0+dfsg-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Thu, 28 Nov 2024 09:06:15 +0100

r-bioc-phyloseq (1.48.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 17 Aug 2024 20:46:13 +0200

r-bioc-phyloseq (1.48.0+dfsg-1~0exp1) experimental; urgency=medium

  * Team upload.
  * d/{,tests/}control: add minimum version number matching bioc-3.19+

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 22 Jul 2024 14:28:01 +0200

r-bioc-phyloseq (1.48.0+dfsg-1~0exp) experimental; urgency=low

  * Team upload
  * New upstream version
  * Set upstream metadata fields: Archive.
  * d/tests/autopkgtest-pkg-r.conf: skip on 32-bit architectures.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 13 Jul 2024 15:04:44 +0200

r-bioc-phyloseq (1.46.0+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 30 Nov 2023 11:27:58 +0100

r-bioc-phyloseq (1.44.0+dfsg-1) unstable; urgency=medium

  * Disable reprotest
  * Mention all Suggests as Test-Depends
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 26 Jul 2023 20:40:16 +0200

r-bioc-phyloseq (1.42.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Reduce piuparts noise in transitions (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 21 Nov 2022 11:18:58 +0100

r-bioc-phyloseq (1.40.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)
  * Remove also README.html since it contains compressed JS

 -- Andreas Tille <tille@debian.org>  Sun, 15 May 2022 10:51:01 +0200

r-bioc-phyloseq (1.38.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  * Manually fix version string of r-cran-ade4

 -- Andreas Tille <tille@debian.org>  Thu, 25 Nov 2021 11:04:44 +0100

r-bioc-phyloseq (1.36.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-Browse.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on r-bioc-biobase,
      r-bioc-biocgenerics, r-bioc-biostrings, r-cran-ape, r-cran-data.table,
      r-cran-foreach, r-cran-ggplot2, r-cran-plyr, r-cran-reshape2,
      r-cran-scales and r-cran-vegan.
  * Provide autopkgtest-pkg-r.conf to make sure testthat will be found
  * Drop debian/tests/control and rely on autopkgtest-pkg-r

 -- Andreas Tille <tille@debian.org>  Fri, 10 Sep 2021 17:32:51 +0200

r-bioc-phyloseq (1.34.0+dfsg-1) unstable; urgency=medium

  [ Dylan Aïssi ]
  * New upstream version
  * debhelper-compat 13 (routine-update)

  [ Andreas Tille ]
  * Standards-Version: 4.5.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 17 Nov 2020 13:23:12 +0100

r-bioc-phyloseq (1.32.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * Testsuite: autopkgtest-pkg-r (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Wed, 20 May 2020 11:20:38 +0200

r-bioc-phyloseq (1.30.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Fixed debian/watch for BioConductor
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g
  * Set upstream metadata fields: Bug-Database.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.

 -- Andreas Tille <tille@debian.org>  Mon, 11 Nov 2019 11:44:23 +0100

r-bioc-phyloseq (1.28.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Thu, 04 Jul 2019 21:38:19 +0200

r-bioc-phyloseq (1.26.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.3.0
  * Remove trailing whitespace in debian/copyright

 -- Andreas Tille <tille@debian.org>  Mon, 14 Jan 2019 11:16:49 +0100

r-bioc-phyloseq (1.26.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.2.1

 -- Dylan Aïssi <daissi@debian.org>  Thu, 15 Nov 2018 22:42:29 +0100

r-bioc-phyloseq (1.24.2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Sun, 22 Jul 2018 02:06:19 +0200

r-bioc-phyloseq (1.24.0+dfsg-1) unstable; urgency=medium

  * Rebuild for R 3.5 transition
  * dh-update-R to update Build-Depends
  * Manually change r-cran-ade4 (>= 1.7.4) to (>= 1.7-4) since
    DESCRIPTION is using wrong version string
  * Remove autogenerated HTML docs since it is including compressed JS

 -- Andreas Tille <tille@debian.org>  Tue, 05 Jun 2018 16:18:05 +0200

r-bioc-phyloseq (1.24.0-2) unstable; urgency=medium

  * Fix Maintainer and Vcs-fields

 -- Andreas Tille <tille@debian.org>  Sun, 06 May 2018 08:35:41 +0200

r-bioc-phyloseq (1.24.0-1) unstable; urgency=medium

  * New upstream version
  * debhelper 11
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * dh-update-R to update Build-Depends
  * Manually fix versioned Depends: r-cran-ade4 (s/1.7.4/1.7-4/)
  * Standards-Version: 4.1.4

 -- Andreas Tille <tille@debian.org>  Fri, 04 May 2018 12:11:03 +0200

r-bioc-phyloseq (1.22.3-1) unstable; urgency=medium

  * New upstream version
  * Secure URI in watch file

 -- Andreas Tille <tille@debian.org>  Sat, 11 Nov 2017 08:37:04 +0100

r-bioc-phyloseq (1.20.0-2) unstable; urgency=medium

  * Standards-Version: 4.1.1

 -- Andreas Tille <tille@debian.org>  Mon, 02 Oct 2017 15:54:31 +0200

r-bioc-phyloseq (1.20.0-1) unstable; urgency=medium

  * New upstream version
  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.0 (nn changes needed)
  * Add debian/README.source to document binary files

 -- Andreas Tille <tille@debian.org>  Fri, 22 Sep 2017 22:56:50 +0200

r-bioc-phyloseq (1.19.1-2) unstable; urgency=medium

  * Fix versioned depends from r-cran-ade4
    Closes: #850767

 -- Andreas Tille <tille@debian.org>  Tue, 10 Jan 2017 09:31:39 +0100

r-bioc-phyloseq (1.19.1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sun, 08 Jan 2017 08:28:07 +0100

r-bioc-phyloseq (1.18.1-1) unstable; urgency=medium

  * New upstream version
  * debhelper 10

 -- Andreas Tille <tille@debian.org>  Sun, 04 Dec 2016 20:59:27 +0100

r-bioc-phyloseq (1.18.0-1) unstable; urgency=medium

  * New upstream version
  * Convert to dh-r
  * Generic BioConductor homepage
  * d/watch: version=4

 -- Andreas Tille <tille@debian.org>  Thu, 17 Nov 2016 16:36:02 +0100

r-bioc-phyloseq (1.16.2-1) unstable; urgency=low

  * Initial release (closes: #837600)

 -- Andreas Tille <tille@debian.org>  Tue, 13 Sep 2016 07:39:43 +0200
